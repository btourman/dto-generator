#!/usr/bin/env node

const program = require('commander');
const fs = require('node-fs');
const _ = require('lodash');
const fetch = require('isomorphic-fetch')

program
    .arguments('<dto_name>')
    .option('-d, --folder <folder>', 'Folder to generate domain')
    .option('-u, --url <url>', 'URL call to get json reponse')
    .option('-a, --auth <auth>', 'Authorization value')
    .action(function (dtoName) {

        const url = program.url
        const auth = program.auth

        const path = getPath(program.folder);

        console.log(getFetchArgs(auth))

        fetch(url, getFetchArgs(auth))
            .then(response => {
                return response.json()
            })
            .then((json = []) => {
                if (Array.isArray(json)) {
                    writeFile(path, json[0], dtoName)
                } else {
                    writeFile(path, json, dtoName)
                }
            })
            .catch(err => {
                console.error('Error in fetch ' + err);
            });

    })
    .parse(process.argv);

function writeFile(dirPath, jsonResponse, dtoName) {
    const keys = Object.keys(jsonResponse)
    const dto = 'export default class {\nconstructor(ob){\n' +
        keys.reduce((acc, key) => {
            acc += key + '=obj.' + key + '\n'
            return acc
        }, '')
        +
        'this.headers=' + JSON.stringify(keys) + '\n}\n}'

    fs.mkdirSync(dirPath, 0777, true);
    const filePath = dirPath + _.capitalize(dtoName) + 'Dto.js';
    // const lines = fs.readFileSync(__dirname + '/templates/' + templateName + '.js', 'utf-8');
    fs.writeFileSync(filePath, dto);
    console.log(filePath + ' created');
}

function getPath(path) {
    if (path) {
        return path + '/';
    }
    return '.';
}

function getFetchArgs(auth) {
    const args = { method: 'GET' }
    if (auth) {
        args.headers = { Authorization: auth }
    }
    return args
}
